import React, { useContext, useEffect, useState } from 'react';
import { Card, Button, Row, Col, Container } from 'react-bootstrap';
import { useParams, useHistory, Link } from 'react-router-dom';
import Swal from 'sweetalert2';
import UserContext from '../UserContext';

export default function ViewCourse() {
  //useParams will allow us to retrieve any parameter in our URL.
  //useParams() will return an object containing our URL params
  const { courseId } = useParams();
  // console.log(courseId);
  //get the global user state from our context
  const { user } = useContext(UserContext);
  //save useHistory and its methods as history variable to use its methods.
  const history = useHistory();

  //save course details in a state
  const [courseDetails, setCourseDetails] = useState({
           name: null,
           description: null,
           price: null,
  });

  //addUseEffect to get course details:
  useEffect(() => {
    fetch(`http://localhost:4000/courses/getSingleCourse/${courseId}`)
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
        if (data._id) {
          const { name, description, price } = data;
          setCourseDetails({
            name,
            description,
            price,
          });
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Course Unavailable',
            text: data.message,
          });
        }
      });
  }, [courseId]);
  console.log(user);
  const enroll = (courseId) => {
    fetch('http://localhost:4000/users/enroll', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
        authorization: `Bearer ${localStorage.getItem('token')}`,
      },
      body: JSON.stringify({
        courseId,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        console.log(data);
      });
  };
  console.log(user);
  const { name, description, price } = courseDetails;
  return (
    <Container>
      <Row>
        <Col>
          <Card>
            <Card.Body className="text-center">
              <Card.Title>{name}</Card.Title>
              <Card.Subtitle>Description:</Card.Subtitle>
              <Card.Text>{description}</Card.Text>
              <Card.Subtitle>Price:</Card.Subtitle>
              <Card.Text>&#8369; {price}</Card.Text>
              <Card.Subtitle>Schedule:</Card.Subtitle>
              <Card.Text>8am - 5pm</Card.Text>
              {user.isAdmin === false ? (
                <Button
                  variant="primary"
                  block
                  onClick={() => enroll(courseId)}
                >
                  Enroll
                </Button>
              ) : (
                <Link
                  variant="primary"
                  class="btn btn-danger btn-block"
                  to="/login"
                  block
                >
                  Login to Enroll
                </Link>
              )}
            </Card.Body>
          </Card>
        </Col>
      </Row>
    </Container>
  );
}
