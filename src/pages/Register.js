import React, { useState, useEffect, useContext } from 'react';
import { Form, Button } from 'react-bootstrap';
//import sweetalert2
import Swal from 'sweetalert2';
import UserContext from '../UserContext';
import { Redirect, useHistory } from 'react-router-dom';

export default function Register() {
  /*
    Review

    Props
       Props is a way to pass data from a parent component to a child component. It is used like an HTML attribute added to the child component. The prop then becomes a property of the special react that all components receive, the props. Prop names are user-defined.

    States
      States are a way to store information within a component. This information can then be updated within the component. When a state is updated through its setter function, it will re-render the component. States are contained within their components. They are independent from other instances of the component.

    Hooks 
      Special/react-defined methods and functions that allow us to do certain tasks in our components.

    useState()
      useState() is a hook that creates states. useState() returns an array with 2 items. The first item in the array is state and the second one is setter function. We then destructure this returned array and assign both items in variables:

      const [stateName,setStateName] = useState(initial value of State)

    useEffect()
      useEffect() hook is used to create effects. These effects will allow us to run a function or task based on when our effect will run. Our useEffect will ALWAYS run on initial render. The next time that the useEffect will run will depend on its dependency array.

      useEffect(()=>{},[dependency array])

      -useEffect will allow us to run a function or task on initial render and whenever our component re-renders IF there is no dependency array.

      -useEffect will allow us to run a function or task on initial render ONLY if there is an EMPTY dependency array.

      -useEffect will allow us to run a function or task on initial render AND whenever the state/s in its dependency array is updated.

      -useEffect will always run on initial render or the very first time our component is displayed or run.

  */

  const [firstName, setFirstName] = useState('');
  const [lastName, setLastName] = useState('');
  const [email, setEmail] = useState('');
  const [mobileNo, setMobileNo] = useState('');
  const [password, setPassword] = useState('');
  const [confirmPassword, setConfirmPassword] = useState('');
  const [isActive, setIsActive] = useState(false);
  const { user } = useContext(UserContext);
  const history = useHistory();

  useEffect(() => {
    if (
      firstName !== '' &&
      lastName !== '' &&
      email !== '' &&
      mobileNo !== '' &&
      password !== '' &&
      confirmPassword === password
    ) {
      setIsActive(true);
    }
  }, [firstName, lastName, email, mobileNo, password, confirmPassword]);

  const registerUser = (e) => {
    e.preventDefault();
    console.log(firstName, lastName);
    fetch('http://localhost:4000/users/', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        firstName,
        lastName,
        email,
        mobileNo,
        password,
      }),
    })
      .then((res) => res.json())
      .then((data) => {
        if (data.email) {
          Swal.fire({
            icon: 'success',
            title: 'Registration Successful',
            text: `Thank you for registering, ${data.email}`,
          });
          history.push('/login');
        } else {
          Swal.fire({
            icon: 'error',
            title: 'Registration Failed.',
            text: data.message,
          });
        }
      });
  };
  return user.id ? (
    <Redirect to="/courses" />
  ) : (
    <>
      <h1 className="my-5 text-center">Register</h1>
      <Form onSubmit={(e) => registerUser(e)}>
        <Form.Group>
          <Form.Label>First Name:</Form.Label>
          <Form.Control
            type="text"
            value={firstName}
            onChange={(e) => setFirstName(e.target.value)}
            placeholder="Enter First Name"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Last Name:</Form.Label>
          <Form.Control
            type="text"
            value={lastName}
            onChange={(e) => setLastName(e.target.value)}
            placeholder="Enter Last Name"
            required
          />
        </Form.Group>{' '}
        <Form.Group>
          <Form.Label>Email:</Form.Label>
          <Form.Control
            type="email"
            value={email}
            onChange={(e) => setEmail(e.target.value)}
            placeholder="Enter Email"
            required
          />
        </Form.Group>{' '}
        <Form.Group>
          <Form.Label>Mobile No:</Form.Label>
          <Form.Control
            type="tel"
            value={mobileNo}
            onChange={(e) => setMobileNo(e.target.value)}
            placeholder="Enter 11 Digit Mobile No."
            required
          />
        </Form.Group>{' '}
        <Form.Group>
          <Form.Label>Password:</Form.Label>
          <Form.Control
            type="password"
            value={password}
            onChange={(e) => setPassword(e.target.value)}
            placeholder="Enter Password"
            required
          />
        </Form.Group>
        <Form.Group>
          <Form.Label>Confirm Password:</Form.Label>
          <Form.Control
            type="password"
            value={confirmPassword}
            onChange={(e) => setConfirmPassword(e.target.value)}
            placeholder="Confirm Password"
            required
          />
        </Form.Group>
        <Button variant="primary" type="submit" disabled={!isActive}>
          Submit
        </Button>
      </Form>
    </>
  );
}
