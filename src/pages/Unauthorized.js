import React from 'react';
import Banner from '../components/Banner';

export default function Unauthorized(props) {
  const pageProps = {
    title: 'Unauthorized Access',
    description: 'The page you are try to access is unavailable',
    buttonCallToAction: 'View our Courses',
    destination: '/',
  };
  return (
    <>
      <Banner bannerProp={pageProps} />
    </>
  );
}
