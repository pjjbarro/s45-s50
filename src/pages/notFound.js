import React from 'react';
import Banner from '../components/Banner';

export default function NotFound(props) {
  const pageProps = {
    title: '404 - Page cannot be found :(',
    description: 'Get your courses online',
    buttonCallToAction: 'Go Home',
    destination: '/',
  };
  return (
    <>
      <Banner bannerProp={pageProps} />
    </>
  );
}
