import React, { useEffect, useContext } from 'react';
import Banner from '../components/Banner';
import UserContext from '../UserContext';

export default function Logout() {
  //destructure the returned object of useContext after unwrapping our context:
  const { unsetUser, setUser } = useContext(UserContext);

  useEffect(() => {
    unsetUser();
    setUser({
      isAdmin: false,
      id: null,
    });
  }, []);
  const bannerContent = {
    title: 'See you later',
    description: 'You have logged out of B123 Booking System',
    buttonCallToAction: 'Go Back To Home Page',
    destination: '/',
  };
  return <Banner bannerProp={bannerContent} />;
}
