import React, { useState, useEffect } from 'react';
//import our mock data
import coursesData from '../data/coursesData';
//import our mock data
import Course from '../components/Course';

export default function Courses() {
  const [coursesCards, setCoursesCards] = useState([]);

  useEffect(() => {
    fetch('http://localhost:4000/courses/getActiveCourses')
      .then((res) => res.json())
      .then((data) => {
        setCoursesCards(
          data.map((course) => <Course key={course._id} courseProp={course} />)
        );
      });
  }, []);
  return (
    <>
      <h1 className="my-5">Available Courses</h1>
      {coursesCards}
    </>
  );
}
