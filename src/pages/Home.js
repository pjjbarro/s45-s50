import React from 'react';
import Banner from '../components/Banner';
import Highlights from '../components/Highlights';

/*
  Home will be a page component, which will be our pages for our application.

  ReactJS adheres to D.R.Y. - Don't Repeat Yourself.

  Props - are data we can pass from a parent component to child component.

  All components actually are able to receive an object, Props are special react objects with which we can pass data around from a parent to child.
*/

export default function Home(props) {
  const sampleProp = {
    title: 'Course Booking System',
    description: 'Get your course online',
    buttonCallToAction: 'Join Us',
    destination: '/login',
  };
  return (
    <>
      <Banner bannerProp={sampleProp} />
      <Highlights />
    </>
  );
}
