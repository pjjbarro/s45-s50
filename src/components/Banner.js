import React from 'react';
import { Row, Col, Button, Jumbotron } from 'react-bootstrap';
import { Link } from 'react-router-dom';

export default function Banner({ bannerProp }) {
  const { title, description, buttonCallToAction, destination } = bannerProp;
  return (
    <Row>
      <Col>
        <Jumbotron>
          <h1>{title}</h1>
          <p>{description}</p>
          <Link
            to={destination ? destination : '/'}
            className="btn btn-primary"
          >
            {buttonCallToAction}
          </Link>
        </Jumbotron>
      </Col>
    </Row>
  );
}
