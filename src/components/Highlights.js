import React from 'react';
import { Row, Col, Card } from 'react-bootstrap';

export default function Highlights() {
  return (
    <Row>
      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Body>
            <Card.Title>
              <h2>Learn from Home</h2>
            </Card.Title>
            <Card.Text>
              Et occaecat est sed sit deserunt ad nisi aliquip est adipisicing
              anim sit eu nulla exercitation cillum fugiat minim consequat in
              nisi qui quis in consectetur exercitation. Cupidatat eiusmod in
              nulla aliqua in veniam incididunt excepteur ea exercitation
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Body>
            <Card.Title>
              <h2>Study Now, Pay Later</h2>
            </Card.Title>
            <Card.Text>
              Et occaecat est sed sit deserunt ad nisi aliquip est adipisicing
              anim sit eu nulla exercitation cillum fugiat minim consequat in
              nisi qui quis in consectetur exercitation.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>

      <Col xs={12} md={4}>
        <Card className="cardHighlights">
          <Card.Body>
            <Card.Title>
              <h2>Be Part of Our Community</h2>
            </Card.Title>
            <Card.Text>
              Et occaecat est cillum fugiat minim consequat in nisi qui quis in
              consectetur exercitation.
            </Card.Text>
          </Card.Body>
        </Card>
      </Col>
    </Row>
  );
}
